﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MyLogger : MonoBehaviour
{
    public static MyLogger Instance;

    [SerializeField] private  int lineCount = 7;
    private Text textComponent;
    private List<string> messages;

    private void Awake()
    {
        Instance = this;
    }
    // Start is called before the first frame update
    void Start()
    {
        textComponent = gameObject.GetComponent<Text>();
        messages = new List<string>();
    }

    public void Log(string msg)
    {
        messages.Add(msg);
        if(messages.Count > lineCount)
        {
            messages.RemoveAt(0);
        }

        textComponent.text = "";

        for(int i=0; i < messages.Count; i++)
        {
            textComponent.text += "\n" + messages[i];
        }
    }
}
