﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARSubsystems;

public class PlaneDetect : MonoBehaviour
{
    public static List<PlaneDetect> Instance;

    private void Awake()
    {
        Instance.Add(this);
    }

    public void Hide()
    {
        gameObject.GetComponent<MeshRenderer>().enabled = false;
        gameObject.GetComponent<MeshCollider>().enabled = false;
        gameObject.GetComponent<ARPlaneMeshVisualizer>().enabled = false;


    }
}
