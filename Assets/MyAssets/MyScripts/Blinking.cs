﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Blinking : MonoBehaviour
{
    public float speed = 1;

    private Material mat;
    private float offset;
    bool breath = true;

    // Start is called before the first frame update
    void Start()
    {
        mat = GetComponent<Renderer>().material;
    }

    // Update is called once per frame
    void Update()
    {
        if (mat == null)
            return;


        offset += (breath? 1:-1) * Time.deltaTime * speed;
        if(offset > 1 || offset <0)
        {
            breath = !breath;
        }

        mat.SetColor("_Color", new Color(255,255,0,offset));
    }
}
