﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARSubsystems;
using UnityEngine.Events;

public class Pointer : MonoBehaviour
{
    public static UnityAction<GameObject> OnChangeObject;

    public Image img, img2;
    public Text txt, txt2;
    int count;
    public GameObject cam;
    public ARRaycastManager RaycastMG;
    static List<ARRaycastHit> hits = new List<ARRaycastHit>();
    static List<ARRaycastHit> hits2 = new List<ARRaycastHit>();

    private GameObject LastHit;
    private Vector2 touch;
    // Start is called before the first frame update
    void Start()
    {
        
        RaycastMG = FindObjectOfType<ARRaycastManager>();
    }


    // Update is called once per frame
    void Update()
    {


        RaycastHit hit;

        if(Physics.Raycast(cam.transform.position, cam.transform.TransformDirection(Vector3.forward), out hit, Mathf.Infinity))
       
        {
            img.color = Color.yellow;
            if (LastHit != hit.collider.gameObject)
            {
                OnChangeObject(hit.collider.gameObject);
                txt.text = hit.collider.gameObject.name;
                count++;
                txt2.text = "" + count;
            }

            LastHit = hit.collider.gameObject;
        }
        else
        {
            img.color = Color.white;

        }
        

        if (Input.touchCount > 0)
        {
            touch = Input.GetTouch(0).position;

            if (RaycastMG.Raycast(touch, hits, TrackableType.Image))
            {
                img2.color = Color.yellow;
            }
            else
            {
                img2.color = Color.white;

            }

        }

    }
}
