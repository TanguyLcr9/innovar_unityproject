﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Interactable : MonoBehaviour
{
   

    private void Awake()
    {
        InputManager.OnTouchObject += OnInteract;
    }

    private void Start()
    {
        
    }

    private void OnDestroy()
    {
        InputManager.OnTouchObject -= OnInteract;
    }

    private void OnInteract(GameObject hit)
    {
        if(hit == gameObject)
        {
            
            if (gameObject.GetComponent<Inventoriable>() != null)
            {
                gameObject.GetComponent<Inventoriable>().OnInventoring();
            }

            if (gameObject.GetComponent<NumberSwitcher>() != null)
            {
                gameObject.GetComponent<NumberSwitcher>().OnSwitch();
            }

            if (gameObject.GetComponent<Showable>() != null)
            {
                gameObject.GetComponent<Showable>().OnShow();
            }

            if (gameObject.GetComponent<ValveBehaviour>() != null)
            {
                gameObject.GetComponent<ValveBehaviour>().OnMouseDown();
            }
        }
    }
}
