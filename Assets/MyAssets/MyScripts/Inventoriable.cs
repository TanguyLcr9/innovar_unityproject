﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[System.Serializable]
public class ObjectData
{
    public string name;
    public Sprite spr;

}

public class Inventoriable : MonoBehaviour
{
    
    public ObjectData ObjData;

    public UnityEvent OnInventory;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    public void OnInventoring()
    { 

        if (Inventory.ActualObj == null)
        {
            OnInventory.Invoke();
            Inventory.Instance.UpdateSlot(ObjData);
            Destroy(gameObject);
        }
        
    }
}
