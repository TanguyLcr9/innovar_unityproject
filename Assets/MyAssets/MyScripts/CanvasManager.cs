﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CanvasManager : MonoBehaviour
{
    public static CanvasManager Instance;
    public GameObject Go_Ferdinand;
    // Start is called before the first frame update
    private void Awake()
    {
            Instance = this;
          
    }

    public void SwitchScene(int scene)
    {

        SceneManager.LoadScene(scene);
    }

    public void ReloadScene()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    public void SwitchParcours()
    {
        if(FindObjectOfType<MaquetteManager>() != null)
        {
            FindObjectOfType<MaquetteManager>().GetComponent<MaquetteManager>().SwitchConcept();
        }
    }
}
