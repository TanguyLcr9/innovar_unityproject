﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ImageInSitu : MonoBehaviour
{
    public Animator animator;

    private void Awake()
    {
        Pointer.OnChangeObject += CheckPointer;
    }

    private void OnDestroy()
    {
        Pointer.OnChangeObject -= CheckPointer;
    }

    void CheckPointer(GameObject hitGo)
    {
        if(hitGo == gameObject)
        {
            animator.SetBool("Over", true);
        }
        else
        {
            if (animator.GetBool("Over") == true)
            {
                animator.SetBool("Over", false);
            }
        }
    }
}
