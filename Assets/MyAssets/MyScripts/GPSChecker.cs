﻿using System.Collections;
using System.Collections.Generic;
using ARLocation;
using UnityEngine;
using UnityEngine.Events;

public class GPSChecker : MonoBehaviour
{


    public double latitude;
    public double longitude;

    public double Radius;

    public bool Debug;

    public AudioClip A_Introduction;

    public HelpData H_OnArriveZone;
    public HelpData H_OnQuitZone;
    public HelpData H_OnScanZone;

    public UnityEvent OnProximity;
    public UnityEvent OnQuitProximity;

    bool state;

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(DoCheckPosition());
        if (!CheckPosition())
        {
            GameManager.Instance.HelpText(H_OnQuitZone);
        }

        AudioManager.Instance.PlayVoice(A_Introduction);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public bool CheckPosition()
    {
        GameObject GPSroot = GameObject.Find("ARLocationRoot");

        if (Radius >= Vector2.Distance(
            new Vector2((float)latitude, (float)longitude),

            new Vector2(
                (float)GPSroot.GetComponent<ARLocationProvider>().CurrentLocation.latitude,
                (float)GPSroot.GetComponent<ARLocationProvider>().CurrentLocation.longitude))
                || Debug)
        {

            return true;
        }
        else
        {
            return false;

        }
    }

    IEnumerator DoCheckPosition()
    {
        while (true)
        {
            //GameObject GPSroot = GameObject.Find("ARLocationRoot");

            if(CheckPosition())
            {
                //MyLogger.Instance.Log("Proximity of the place");



                if (!state)
                {
                    OnProximity.Invoke();
                    StartCoroutine(DoHelp());
                    state = true;
                }

            }
            else
            {
                //MyLogger.Instance.Log("Too Far");

                if (state)
                {
                    OnQuitProximity.Invoke();
                    GameManager.Instance.HelpText(H_OnQuitZone);
                    state = false;
                }
            }

            //MyLogger.Instance.Log(GPSroot.GetComponent<ARLocationProvider>().CurrentLocation.latitude.ToString() + " , " +
               //GPSroot.GetComponent<ARLocationProvider>().CurrentLocation.longitude.ToString());

            yield return new WaitForSeconds(2f);
        }
    }

    IEnumerator DoHelp()
    {
        yield return new WaitForSeconds(1f);

        GameManager.Instance.HelpText(H_OnArriveZone);

        yield return new WaitForSeconds(10f);

        GameManager.Instance.HelpText(H_OnScanZone);
    }
}
