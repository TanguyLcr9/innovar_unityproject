﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;



public class BoulonManager : MonoBehaviour
{
    public int ScrewsNumber;

    public ObjectData KeyWorkData;

    public HelpData H_OnHelpKey;

    public AudioClip A_AudioToPlay;

    public AudioClip A_Trajet;

    public UnityEvent OnOpen;

    public string AnimationEnd;

    public ObjectData KeyData;

    public GameObject Dummy;

    private int screwsRemoved;

    private List<bool> action = new List<bool>();

    private void Start()
    {
        for (int i = 0; i < 3; i++)
        {
            action.Add(false);
        }

        screwsRemoved = 0;

        Inventory.Instance.UpdateSlot(KeyData);

        GameManager.Instance.HelpText(H_OnHelpKey);

        AudioManager.Instance.PlayVoice(A_AudioToPlay);
    }

    public void DoUnscrew()
    {
        Inventory.Instance.UpdateSlot(KeyWorkData);

        screwsRemoved++;

        if(screwsRemoved == ScrewsNumber)
        {
            //Do Event
            OnOpen.Invoke();
            Inventory.Instance.UpdateSlot(null);
        }
    }

    public void CheckPlayerComplete(int actionNum)
    {

        action[actionNum] = true;

        for (int i = 0; i < 3; i++)
        {
            if (!action[i])
            {
                return;
            }
        }

        StartCoroutine(DoQuit());

    }

    IEnumerator DoQuit()
    {

        GetComponent<Animator>().Play(AnimationEnd);

        yield return new WaitForSeconds(5f);

        AudioManager.Instance.PlayVoice(A_Trajet);

        Destroy(Dummy);
    }

}
