﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class NumberSwitcher : MonoBehaviour
{
    //[SerializeField]
    //private TextMesh txt;

    [SerializeField]
    private Renderer ChiffreRend;

    public int number;

    public float offset = 0;

    private NumberManager numManager;

    private void Start()
    {
        numManager = transform.GetComponentInParent<NumberManager>();

    }

    public void OnSwitch()
    {
        number++;
        offset -= 0.1f;
        //mat.SetTextureOffset("_MainTex", new Vector2(0,offset));
        ChiffreRend.material.DOOffset(new Vector2(0, offset), "_MainTex", 0.2f);

        if (number > 9)
        {
            number = 0;
            
        }

        //txt.text = number.ToString();

        numManager.CheckCode();
    }
}
