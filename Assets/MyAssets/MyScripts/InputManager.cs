﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARSubsystems;
using UnityEngine.Events;

public class InputManager : MonoBehaviour
{
    public static InputManager Instance;

    public static UnityAction<GameObject> OnTouchObject;

    //servira a stocker la position en 2D sur laquelle vous taperez sur votre ecran
    private Vector2 tapPosition;

    private Camera cam;

    //servira a stocker toutes les intersections entre un rayon et les plans
    //il est possible d'en avoir plusieurs points par ray cast
    //static List<ARRaycastHit> hits = new List<ARRaycastHit>();

    RaycastHit hit;

    private void Awake()
    {
        Instance = this;
    }

    // Start is called before the first frame update
    void Start()
    {
        //MyLogger.Instance.Log("Interaction enabled");
        cam = Camera.main;
    }

    // Update is called once per frame
    void Update()
    {
        //si il n'y a pas de doigt sur l'ecran
        if (!getTapPos(out Vector2 tapPosition))
        {
            //on quitte la fonction update (donc on s'arrete ici)
            return;
        }

        if (Input.GetTouch(0).phase == TouchPhase.Began)
        {

            if (Physics.Raycast(cam.ScreenPointToRay(tapPosition), out hit))
            {
                OnTouchObject(hit.collider.gameObject);
                MyLogger.Instance.Log(hit.collider.gameObject.name);
            }

        }
    }

    bool getTapPos(out Vector2 tapPosition)
    {
        if (Input.touchCount > 0)
        {
            tapPosition = Input.GetTouch(0).position;
            
            return true;
        }

        tapPosition = default;
        return false;
    }
}
