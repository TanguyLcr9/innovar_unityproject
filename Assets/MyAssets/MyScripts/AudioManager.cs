﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AudioManager : MonoBehaviour
{
    public static AudioManager Instance;

    [SerializeField]
    private GameObject Ferdinand;

    [SerializeField]
    private GameObject SoundChecker;

    private Image img;

    private AudioSource source;


    private void Awake()
    {
        Instance = this;
    }

    // Start is called before the first frame update
    void Start()
    {
        img = GetComponent<Image>();
        source = GetComponent<AudioSource>();

    }

    public void PlayVoice(AudioClip clip, bool DoWait=true)
    {
        if (DoWait)
        {
            StartCoroutine(DoVoice(clip));
        }
        else
        {
            PlayClip(clip);
        }
    }

    private void PlayClip(AudioClip clip)
    {
        source.clip = clip;
        source.Play();
    }

    IEnumerator DoVoice(AudioClip clip)
    {
        PlayClip(clip);

        Display(true);

        yield return new WaitForSeconds(clip.length);

        Display(false);
    }

    void Display(bool state)
    {
        Ferdinand.SetActive(state);
        SoundChecker.SetActive(state);
        img.enabled = state;
    }
}
