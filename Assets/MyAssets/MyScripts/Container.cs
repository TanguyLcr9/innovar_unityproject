﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Container : MonoBehaviour
{
    public Material EmptyMaterial;

    public Material OverMaterial;

    public string ObjNameRequire;

    public UnityEvent OnPlacingDone;

    public bool IsEmpty;

    public bool ShowHelp;
    public HelpData H_HelpPutting;

    private Material baseMaterial;

    private Renderer rend;


    // Start is called before the first frame update
    void Start()
    {
        rend = GetComponent<Renderer>();
        baseMaterial = rend.material;
        rend.material = EmptyMaterial;

        IsEmpty = true;

        if (GetComponent<Interactable>() != null)
        {
            GetComponent<Interactable>().enabled = false;
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void OnPlaceObject()
    {
        rend.material = baseMaterial;
        IsEmpty = false;

        if(ShowHelp)
        GameManager.Instance.HelpText(H_HelpPutting);

        OnPlacingDone.Invoke();

        if (GetComponent<Interactable>() != null)
        {
            GetComponent<Interactable>().enabled = true;
        }

        if (GetComponent<ValveBehaviour>() != null)
        {
            GetComponent<ValveBehaviour>().enabled = true;
        }
    }
}
