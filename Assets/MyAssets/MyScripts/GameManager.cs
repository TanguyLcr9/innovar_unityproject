﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public static GameManager Instance;

    public ObjectData ActualObj = null;

    // Start is called before the first frame update
    void Awake()
    {
        if(Instance == null)
        {
            //ActualObj = null;
            Instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }

        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void HelpText(HelpData data, bool Hide = false)
    {

        if (HelpManager.Instance != null)
        {
            if (Hide)
            {
                HelpManager.Instance.ChangeHelp(data);
            }
            else
            {
                StartCoroutine(HelpManager.Instance.ShowText(data));
            }
        }
    }

}
