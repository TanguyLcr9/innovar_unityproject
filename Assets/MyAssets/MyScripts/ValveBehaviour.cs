﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using DG.Tweening;

public class ValveBehaviour : MonoBehaviour
{
    public GameObject Pont;
    public GameObject ObjectNextTo;
    public GameObject Nacelle;
    public float speedValue = 0.3f;
    public float duration = 0.4f;
    public float LimiteMin, LimiteMax;

    public Vector3 Offset;
    public Vector3 V3angle;

    public HelpData H_HelpValve;

    public AudioClip A_OnStart;
    public AudioClip A_OnEnd;

    public ObjectData ValveData;

    Camera cam;
    bool IsTruning;
    float lastAngle;
    float angle;
    float angleOffset;
    int sensDeDeplacement;//varie entre -1, 0 et 1 pour donner le sens de la rotation ou bien 0 si immobile

    // Start is called before the first frame update
    void Start()
    {
        cam = Camera.main;
        StartCoroutine(MoveNacelle());
        //Pont = Instantiate(Pont,GameObject.Find("ARLocationRoot").transform);
        
        Inventory.Instance.UpdateSlot(ValveData);

        

        StartCoroutine(WaitToPose());

        GameManager.Instance.HelpText(H_HelpValve);

        AudioManager.Instance.PlayVoice(A_OnStart);
    }

    IEnumerator WaitToPose()
    {
        yield return new WaitForSeconds(1f);
        GameObject inst = Instantiate(ObjectNextTo, transform.position + transform.TransformDirection(Offset), Quaternion.identity);
        inst.transform.eulerAngles = V3angle;
        Nacelle = GameObject.Find("Nacelle");

    }

    // Update is called once per frame
    void Update()
    {
        if (GetComponent<Container>().IsEmpty)
        {
            return;
        }
        if (IsTruning)
        {
            Vector3 camPoint = cam.WorldToScreenPoint(transform.position);


            Vector2 camPoint_V2 = new Vector2(camPoint.x, camPoint.y);
            Vector2 mouse_V2 = Vector2.zero;

            if (Application.platform == RuntimePlatform.WindowsEditor)
            {
                Vector3 mouse = Input.mousePosition;
                mouse_V2 = new Vector2(mouse.x, mouse.y);
            }

            if (Application.platform == RuntimePlatform.Android)
            {
                mouse_V2 = Input.GetTouch(0).position;
            }

            angle = Vector2.Angle((camPoint_V2 - mouse_V2).normalized, Vector2.down);

            if (camPoint_V2.x > mouse_V2.x)
                angle = 360 - angle;

            transform.eulerAngles = new Vector3(transform.eulerAngles.x, transform.eulerAngles.y, angleOffset - angle);

            if (lastAngle < 90 && angle > 270)
            {
                Debug.Log("Sens inverse des aiguilles d'une montre");
                sensDeDeplacement = -1;
            }

            if (angle < 90 && lastAngle > 270)
            {
                Debug.Log("Aiguilles d'une montre");
                sensDeDeplacement = 1;

            }

            lastAngle = angle;
            if (Application.platform == RuntimePlatform.Android)
            {
                if (Input.GetTouch(0).phase == TouchPhase.Ended)
                {
                    IsTruning = false;
                }
            }
        }
    }

    public void OnMouseDown()
    {
        IsTruning = true;
        angleOffset = angle + transform.eulerAngles.z;
    }

    public void OnMouseUp()
    {
        IsTruning = false;

    }


    IEnumerator MoveNacelle()
    {


        while (true)
        {
            switch (sensDeDeplacement)
            {
                case -1:

                    if (Nacelle.transform.localPosition.x > LimiteMin)
                    {
                        Nacelle.transform.DOLocalMoveX((speedValue * sensDeDeplacement) + Nacelle.transform.localPosition.x, duration);
                    }

                    break;
                case 1:

                    if (Nacelle.transform.localPosition.x < LimiteMax)
                    {
                        Nacelle.transform.DOLocalMoveX((speedValue * sensDeDeplacement) + Nacelle.transform.localPosition.x, duration);

                    }
                    else
                    {
                        AudioManager.Instance.PlayVoice(A_OnEnd);
                        StartCoroutine(LoadingScreen.Instance.DoFadeScreen(1));
                        enabled = false;
                    }

                    break;
            }

            sensDeDeplacement = 0;
            yield return new WaitForSeconds(duration);
        }
       
    }
}

