﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARSubsystems;

[RequireComponent(typeof(ARAnchorManager))]
public class AnchorContentSpwaner : MonoBehaviour
{

    [SerializeField] private GameObject Content;
    [SerializeField] private Camera camera;

    private List<ARAnchor> m_Anchors;
    private ARAnchorManager m_AnchorManager;
    // Start is called before the first frame update
    void Start()
    {
        m_AnchorManager = GetComponent<ARAnchorManager>();
        m_Anchors = new List<ARAnchor>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.touchCount == 0)
            return;

        var touch = Input.GetTouch(0);
        if (touch.phase != TouchPhase.Began)
            return;

        var anchor = m_AnchorManager.AddAnchor(
            new Pose(camera.ScreenToWorldPoint(new Vector3(touch.position.x, touch.position.y, 0.3f)),
            Quaternion.identity)
            );

        if(anchor != null)
        {
            m_Anchors.Add(anchor);
            Instantiate(Content, anchor.transform);
        }
    }
}
