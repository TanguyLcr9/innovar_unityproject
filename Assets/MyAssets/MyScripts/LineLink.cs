﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LineLink : MonoBehaviour
{
    public LineRenderer lineRenderer;
    public Transform T_BottomImage;

    // Update is called once per frame
    void Update()
    {
        lineRenderer.SetPosition(1, T_BottomImage.position - lineRenderer.transform.position);
    }
}
