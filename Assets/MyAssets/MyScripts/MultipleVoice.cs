﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MultipleVoice : MonoBehaviour
{

    public AudioClip A_Audio1;
    public AudioClip A_Audio2;

    public float TimeBetween;

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(WaitBetween());
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    IEnumerator WaitBetween()
    {
        AudioManager.Instance.PlayVoice(A_Audio1);

        yield return new WaitForSeconds(TimeBetween);

        AudioManager.Instance.PlayVoice(A_Audio2);

    }
}
