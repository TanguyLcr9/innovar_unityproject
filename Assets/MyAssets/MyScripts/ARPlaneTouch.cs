﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARSubsystems;

public class ARPlaneTouch : MonoBehaviour
{
    //variable exposee dans l'inspecteur pour que l'on puisse drag and drop un prefab dedans
    [SerializeField] GameObject spawnablePrefab;

    //servira a stocker l'objet une fois cree
    private GameObject spawnedGObj;
    //servira a stocker une reference pour manipuler le AR Raycast Manager
    private ARRaycastManager raycastMg;
    //servira a stocker la position en 2D sur laquelle vous taperez sur votre ecran
    private Vector2 tapPosition;

    //servira a stocker toutes les intersections entre un rayon et les plans
    //il est possible d'en avoir plusieurs points par ray cast
    static List<ARRaycastHit> hits = new List<ARRaycastHit>();

    //quand le gameObject "prend vie"...
    void Awake()
    {
        //on recupere stocke la reference sur le AR Raycast Manager
        raycastMg = GetComponent<ARRaycastManager>();
    }

    //a chaque nouvelle frame...
    void Update()
    {
        //si il n'y a pas de doigt sur l'ecran
        if (!getTapPos(out Vector2 tapPosition))
        {
            //on quitte la fonction update (donc on s'arrete ici)
            return;
        }

        //si on tracant un rayon on touche une surface (de type PlaneWithinPolygon)
        //au passage les intersection de ce trace sont stockee dans la variable hits
        if (Input.GetTouch(0).phase == TouchPhase.Began && raycastMg.Raycast(tapPosition, hits, TrackableType.PlaneWithinPolygon))
        {
            //on recupere la premiere intersection dans une variable hitPose
            Pose hitPose = hits[0].pose;

            //si l'objet n'as pas encore ete cree
            if (spawnedGObj == null)
            {
                //on creer un objet a partir du prefab a l'intersection entre le rayon et la surface
                //puis on stocke une reference sur cet objet
                spawnedGObj = Instantiate(spawnablePrefab, hitPose.position, hitPose.rotation);
                Destroy(this);
            }
            /*
            else //...sinon
            {
                //on utilise la reference sur l'objet pour modifier son emplacement
                //qui sera maintenant le meme que l'intersection entre le plan et la surface
                spawnedGObj.transform.position = hitPose.position;
            }
            */
        }
    }

    //cette fonction permet de savoir si et où un doigt a touche l'ecran
    //(a noter que le "où" est recupere en effectuant une mutation sur le parametre via "out")
    bool getTapPos(out Vector2 tapPosition)
    {
        if (Input.touchCount > 0)
        {
            tapPosition = Input.GetTouch(0).position;
            return true;
        }

        tapPosition = default;
        return false;
    }
}