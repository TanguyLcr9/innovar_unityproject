﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MaquetteManager : MonoBehaviour
{
    public GameObject Concept1;
    public GameObject Concept2;

    public Text ButtonText;

    private bool State = true;

    public void Start()
    {
        ButtonText = GameObject.Find("ParcoursText").GetComponent<Text>();
        SwitchConcept();

        MyLogger.Instance.Log("Hola");
    }
    public void SwitchConcept()
    {
        Concept1.SetActive(State);
        Concept2.SetActive(!State);

        ButtonText.text = State ? "Parcours 1" : "Parcours 2";

        State = !State;
    }
}
