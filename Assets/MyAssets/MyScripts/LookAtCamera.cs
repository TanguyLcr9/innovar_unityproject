﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LookAtCamera : MonoBehaviour
{
    private Camera cam;

    private void Start()
    {
        cam = FindObjectOfType<Camera>();
    }
    // Update is called once per frame
    void Update()
    {
        transform.LookAt(cam.transform);
    }
}
