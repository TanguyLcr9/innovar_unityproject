﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveTexture : MonoBehaviour
{
    public float speed = 1;

    private Material mat;
    private float offset;
    // Start is called before the first frame update
    void Start()
    {
        mat = GetComponent<Renderer>().material;
    }

    // Update is called once per frame
    void Update()
    {
        if (mat == null)
            return;
        offset += Time.deltaTime * speed;
              
        mat.SetTextureOffset("_MainTex", new Vector2(offset,0.0f));
    }
}
