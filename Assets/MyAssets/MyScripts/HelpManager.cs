﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public class HelpData
{
    [TextArea]
    public string text;
    public Sprite spr;
}

public class HelpManager : MonoBehaviour
{
    public static HelpManager Instance;

    public float Duration = 3f;

    public bool ShowOnStart;

    [SerializeField]
    private Text txt;

    [SerializeField]
    private Image img;

    [SerializeField]
    private Image imgArrow;

    private Image panel;


    private void Awake()
    {
        Instance = this;
    }

    private void Start()
    {
        panel = GetComponent<Image>();
        ActivePanel(false);

        if(ShowOnStart){

            DisplayText();

            }
    }

    public void DisplayText()
    {
        if(txt.enabled == true)
        {
            return;
        }
        HelpData ThisData = new HelpData();

        ThisData.text = txt.text;
        ThisData.spr = img.sprite;

        StartCoroutine(ShowText(ThisData));
    }

    public IEnumerator ShowText(HelpData data)
    {

        ActivePanel(true);
        ChangeHelp(data);
        yield return new WaitForSeconds(Duration);
        ActivePanel(false);

    }

    public void ChangeHelp(HelpData data)
    {
        txt.text = data.text;
        img.sprite = data.spr;
    }

    void ActivePanel(bool state)
    {
        panel.enabled = state;
        txt.enabled = state;
        img.enabled = state;
        imgArrow.enabled = state;
    }
}
