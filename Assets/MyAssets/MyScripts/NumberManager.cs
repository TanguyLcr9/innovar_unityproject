﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class NumberManager : MonoBehaviour
{
    public List<NumberSwitcher> number;
    public List<int> correctCode;
    public UnityEvent OnValide;

    private void Start()
    {
        for(int i = 0; i < transform.childCount; i++)
        {
            number.Add(transform.GetChild(i).GetComponent<NumberSwitcher>());
        }
    }

    public void CheckCode()
    {
        // Si un des chiffre est mauvais alors on retourne
        for (int i=0;i< number.Count ; i++)
        {
            if(number[i].number != correctCode[i])
            {
                return;
            }
        }

        //Désactive les chiffres
        for (int i = 0; i < number.Count; i++)
        {
            number[i].GetComponent<Interactable>().enabled = false;
        }

        //
            OnValide.Invoke();
            MyLogger.Instance.Log("Good Code");
        
    }
}
