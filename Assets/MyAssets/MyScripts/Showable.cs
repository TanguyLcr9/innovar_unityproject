﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Showable : MonoBehaviour
{
    Camera cam;

    Transform parent;

    public bool DestroyAfter;

    public bool ShowOnStart;

    public bool SaveIndice;
    
    public HelpData H_Indice;

    public AudioClip A_AudioToPlay;

    public UnityEvent OnShowEvent;

    public UnityEvent OnUnshow;

    // Start is called before the first frame update
    void Start()
    {
        cam = Camera.main;
        parent = transform.parent.transform;
        if(ShowOnStart)
        StartCoroutine(DoShow());
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void OnShow()
    {
        OnShowEvent.Invoke();
        StartCoroutine(DoShow());
    }

    IEnumerator DoShow()
    {
        transform.position = cam.transform.GetChild(0).transform.position;
        transform.rotation = cam.transform.GetChild(0).transform.rotation;
        transform.SetParent(cam.transform.GetChild(0).transform);

        GetComponent<Interactable>().enabled = false;

        //Changer l'aide
        if(SaveIndice)
        GameManager.Instance.HelpText(H_Indice, true);

        yield return new WaitForSeconds(1f);

        if(Application.platform == RuntimePlatform.Android){

            InputManager.Instance.enabled = false;

            if (A_AudioToPlay !=null){

                AudioManager.Instance.PlayVoice(A_AudioToPlay);
                yield return new WaitForSeconds(A_AudioToPlay.length);

                }else{
                
                yield return new WaitUntil(()=> Input.touchCount > 0);

                }

            InputManager.Instance.enabled = true;
        }

        transform.SetParent(parent);
        transform.position = parent.position;
        transform.rotation = parent.rotation;

        if (DestroyAfter)
        {
            OnUnshow.Invoke();
            Destroy(gameObject);
        }

        GetComponent<Interactable>().enabled = true;
    }
}
