﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.ARFoundation;

[RequireComponent(typeof(ARTrackedImageManager))]
public class ImageMarkerPair : MonoBehaviour
{
    [SerializeField]
    private GameObject[] prefabs;

    private Dictionary<string, GameObject> spawnedGameObj = new Dictionary<string, GameObject>();
    private ARTrackedImageManager imgManager;

    private void Awake()
    {
        imgManager = FindObjectOfType<ARTrackedImageManager>();

        foreach (GameObject gObj in prefabs)
        {
            GameObject newGObj = Instantiate(gObj, Vector3.zero, Quaternion.identity);
            newGObj.SetActive(false);
            newGObj.name = gObj.name;
            spawnedGameObj.Add(gObj.name, newGObj);
        }

    }

    private void OnEnable()
    {
        imgManager.trackedImagesChanged += ImageChanged;
    }

    private void OnDisable()
    {
        imgManager.trackedImagesChanged -= ImageChanged;
    }

    private void ImageChanged(ARTrackedImagesChangedEventArgs eventArgs)
    {
        foreach (ARTrackedImage trackedImage in eventArgs.added)
        {
            UpdateImage(trackedImage);
        }
        foreach (ARTrackedImage trackedImage in eventArgs.updated)
        {
            UpdateImage(trackedImage);
        }
        foreach (ARTrackedImage trackedImage in eventArgs.removed)
        {
            spawnedGameObj[trackedImage.name].SetActive(false);
        }
    }

    private void UpdateImage(ARTrackedImage trackedImage)
    {

        string name = trackedImage.referenceImage.name;
        Vector3 position = trackedImage.transform.position;
        Vector3 rotation = trackedImage.transform.eulerAngles;

        GameObject prefab = spawnedGameObj[name];
        prefab.transform.position = position;
        prefab.transform.eulerAngles = rotation;
        prefab.SetActive(true);
    }
}
