﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class DragImage : MonoBehaviour , IDragHandler , IEndDragHandler
{
    private BoxCollider2D col;

    private Camera cam;

    private RaycastHit hit;

    private void Start()
    {
        col = GetComponent<BoxCollider2D>();
        cam = Camera.main;
    }

   
    public void OnDrag(PointerEventData eventData)
    {
        //Fonctionne pour pc mais à voir sur un smartphone
        transform.position = eventData.position;
    
    }

    public void OnEndDrag(PointerEventData eventData)
    {

        transform.localPosition = Vector3.zero;

        //Appliquer au point de relachement
        if (Physics.Raycast(cam.ScreenPointToRay(eventData.position), out hit))
        {
            //Si c'est un objet de type container
            if (hit.collider.gameObject.GetComponent<Container>() != null)
            {
                //Si c'est le bon container en comparant deux strings celui de l'inventaire et celui du coneteneur réquis
                if (hit.collider.gameObject.GetComponent<Container>().ObjNameRequire == Inventory.ActualObj.name)
                {
                    //On place l'objet dans son contenant
                    hit.collider.gameObject.GetComponent<Container>().OnPlaceObject();

                    //Exeption pour les boulons
                    if (!hit.collider.gameObject.CompareTag("Boulon"))
                    {
                        //On update les infos dans l'inventaire pour le rendre vide
                        Inventory.Instance.UpdateSlot(null);
                    }
                }
            }
        }
        
        
    }
}
