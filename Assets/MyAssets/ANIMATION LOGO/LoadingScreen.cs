﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.SceneManagement;

public class LoadingScreen : MonoBehaviour
{
    public static LoadingScreen Instance;

    public bool OnStart = true;

    private void Awake()
    {
        Instance = this;
    }

    private void Start()
    {


        if (OnStart)
        {
            StartCoroutine(DoFadeScreen(0));
        }
        else
        {
            gameObject.SetActive(false);
            GetComponent<CanvasGroup>().alpha = 0;

        }
    }

    public IEnumerator DoFadeScreen(int alpha)
    {
        
        if(alpha == 1)
        {
            gameObject.SetActive(true);
        }

        yield return new WaitForSeconds(2f);
        
        yield return GetComponent<CanvasGroup>().DOFade(alpha, 1f).WaitForCompletion();

            if(alpha == 0)
            {
                Destroy(gameObject);
            }

    }
}
