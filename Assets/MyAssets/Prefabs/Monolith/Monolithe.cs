﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARSubsystems;
using UnityEngine.SceneManagement;

public class Monolithe : MonoBehaviour
{
    public HelpData H_OnMonolithStart;

    public string QuitAnimation;

    public AudioClip A_Etape1;

    private List<bool> action = new List<bool>();

    // Start is called before the first frame update
    void Start()
    {
        for (int i = 0; i < 3; i++)
        {
            action.Add(false);
        }

        GameManager.Instance.HelpText(H_OnMonolithStart);

        AudioManager.Instance.PlayVoice(A_Etape1);

        /*foreach (PlaneDetect Instances in PlaneDetect.Instance)
        {
            Instances.Hide();
        }
        PointCloud.Instance.Hide();
        FindObjectOfType<ARPointCloudManager>().GetComponent<ARPointCloudManager>().enabled = false;
        FindObjectOfType<ARPointCloudManager>().GetComponent<ARPointCloudManager>().pointCloudPrefab = null;
        FindObjectOfType<ARPlaneManager>().GetComponent<ARPlaneManager>().enabled = false;
        FindObjectOfType<ARPlaneManager>().GetComponent<ARPlaneManager>().planePrefab = null;*/
    }

    public void CheckPlayerComplete(int actionNum){

        action[actionNum] = true;

        for(int i = 0; i < 3; i++)
        {
            if (!action[i])
            {
                return;
            }
        }

        StartCoroutine(DoQuitMonolith());

    }

    IEnumerator DoQuitMonolith()
    {

        yield return new WaitForSeconds(3f);

        GetComponent<Animator>().Play(QuitAnimation);

        HelpManager.Instance.DisplayText();

        yield return new WaitForSeconds(6f);

        SceneManager.LoadScene(1);
    }

}
